(function($){
  $(function(){

    $('.sidenav').sidenav();
    $('.parallax').parallax();
    $('.datepicker').datepicker();
    $('.fixed-action-btn').floatingActionButton();
    $('.tooltipped').tooltip();

  }); // end of document ready
})(jQuery); // end of jQuery name space
